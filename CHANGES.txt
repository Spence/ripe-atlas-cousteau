v<0.5>, < 2014-05-22> -- 0.5 release.
CHANGES:
- change package structure to comply with the new structure of atlas packages
- add continuous intergration supoport
  - add tests
  - enable travis
  - enable code health checks
- add required files for uploading to github
v<0.4>, < 2014-03-31> -- 0.4 release.
NEW FEATURES:
- add support for stopping a measurement.
v<0.3>, < 2014-02-25> -- 0.3 release.
NEW FEATURES:
- add simple support for HTTP GET queries.
v<0.2>, < 2014-02-03> -- 0.2 release.
NEW FEATURES:
- add support for adding/removing probes API request.
CHANGES:
- use AtlasCreateRequest instead of AtlasRequest for creating a new measurement.
v<0.1>, < 2014-01-21> -- Initial release.
